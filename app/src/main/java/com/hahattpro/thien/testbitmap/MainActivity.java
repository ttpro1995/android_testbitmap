package com.hahattpro.thien.testbitmap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import org.apache.http.protocol.HTTP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {


    ImageView imageView1;
    ImageView imageView2;
    Bitmap bitmap1;
    Bitmap bitmap2;
    Bitmap bitmap3;

    OutputStream os1;
    OutputStream os2;
    OutputStream os3;

    File file1;
    File file2;
    File file3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        new BitmapAsyn().execute();


    }

    public class BitmapAsyn extends AsyncTask<Void,Void,Void>{



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try{
            InputStream is = getResources().openRawResource(R.raw.hieu);
          bitmap1 = BitmapFactory.decodeStream(is);

            //bitmap1 =  MeowBitmapEditor.BoxBlurImprove(bitmap1,10,2);
                bitmap1 =  MeowBitmapEditor.MirrorLeftToRight(bitmap1);
                //
                file1 = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"meow_bmp1.png");
                os1 = new FileOutputStream(file1);
                bitmap1.compress(Bitmap.CompressFormat.JPEG,100,os1);
            }
            catch (Exception e){e.printStackTrace();};


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            imageView1.setImageBitmap(bitmap1);
            SendMail("test1");
        }


    }

    public void SendMail(String subject){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        Log.i("meow", "path = " + file1.getPath());
// The intent does not have a URI, so declare the "text/plain" MIME type
        emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"testing.ttpro1995@yahoo.com.vn"}); // recipients
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "keep calm and meow on");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file1));
        startActivity(emailIntent);
    }


}
